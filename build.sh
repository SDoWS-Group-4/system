#!/bin/bash

set -e
docker-compose ps
if [ $? = 0 ]; then
    
    echo "Stopping System services"

    docker-compose down
    docker-compose kill

    # prune       Remove unused data
    # -a Remove all unused images not just dangling ones
    # -f Do not prompt for confirmation
    docker system prune -f

    echo "Removing build cache"
fi

echo "Starting docker-compose"
docker-compose up -d

